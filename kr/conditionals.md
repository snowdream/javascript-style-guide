## Conditional Expressions & Equality(조건식과 등가식)

  - `==` 나 `!=` 보다는 `===` 와 `!==` 를 사용해 주십시오
  - 조건식은`ToBoolean` 메소드에 의해 엄밀하게 비교됩니다. 항상 이 간단한 규칙에 따라 주십시오.

    + **Objects** 는 **true** 로 평가됩니다.
    + **undefined** 는 **false** 로 평가됩니다.
    + **null** 는 **false** 로 평가됩니다.
    + **Booleans** 는 **boolean형의 값** 으로 평가됩니다.
    + **Numbers** 는 **true** 로 평가됩니다. 하지만 **+0, -0, or NaN** 의 경우는 **false** 입니다.
    + **Strings** 는 **true** 로 평가됩니다. 하지만 빈문자 `''` 의 경우는 **false** 입니다.


    ```javascript
    if ([0]) {
      // true
      // Array는 Object 이므로 true 로 평가됩니다.
    }
    ```

  - 짧은형식을 사용하십시오.

    ```javascript
    // bad
    if (name !== '') {
      // ...stuff...
    }

    // good
    if (name) {
      // ...stuff...
    }

    // bad
    if (collection.length > 0) {
      // ...stuff...
    }

    // good
    if (collection.length) {
      // ...stuff...
    }
    ```

  - 더 자세한 정보는 Angus Croll 의 [Truth Equality and JavaScript](http://javascriptweblog.wordpress.com/2011/02/07/truth-equality-and-javascript/#more-2108)를 참고해 주십시오.