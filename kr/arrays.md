## Arrays

  - 배열을 만들 때 리터럴 구문을 사용하십시오.

    ```javascript
    // bad
    var items = new Array();

    // good
    var items = [];
    ```

  - 길이를 알 수없는 경우는 Array#push를 사용하십시오.
  
    ```javascript
    var someStack = [];


    // bad
    someStack[someStack.length] = 'abracadabra';

    // good
    someStack.push('abracadabra');
    ```

  - 배열을 복사 할 필요가있는 경우 Array#slice를 사용하십시오. [jsPerf](http://jsperf.com/converting-arguments-to-an-array/7)

    ```javascript
    var len = items.length,
        itemsCopy = [],
        i;

    // bad
    for (i = 0; i < len; i++) {
      itemsCopy[i] = items[i];
    }

    // good
    itemsCopy = items.slice();
    ```

  - Array와 비슷한(Array-Like)한 Object를 Array에 변환하는 경우는 Array#slice를 사용하십시오. 

    ```javascript
    function trigger() {
      var args = Array.prototype.slice.call(arguments);
      ...
    }
    ```