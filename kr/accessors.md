## Accessors

  - 속성을 위한 접근자(Accessor) 함수는 필요 없습니다.
  - 접근자 함수가 필요한 경우 `getVal()` 이나 `setVal('hello')` 라고 사용합니다.

    ```javascript
    // bad
    dragon.age();

    // good
    dragon.getAge();

    // bad
    dragon.age(25);

    // good
    dragon.setAge(25);
    ```

  - 속성이 boolean의 경우 `isVal()` 이나 `hasVal()` 라고 사용합니다.
    ```javascript
    // bad
    if (!dragon.age()) {
      return false;
    }

    // good
    if (!dragon.hasAge()) {
      return false;
    }
    ```

  - 일관된다면 `get()` 이나 `set()` 이라는 함수를 작성해도 좋습니다.

    ```javascript
    function Jedi(options) {
      options || (options = {});
      var lightsaber = options.lightsaber || 'blue';
      this.set('lightsaber', lightsaber);
    }

    Jedi.prototype.set = function(key, val) {
      this[key] = val;
    };

    Jedi.prototype.get = function(key) {
      return this[key];
    };
    ```
