## Functions

  - 함수식(Function expressions)

    ```javascript
    // 익명함수식(anonymous function expression)
    var anonymous = function() {
      return true;
    };

    // 명명된 함수식(named function expression)
    var named = function named() {
      return true;
    };

    // 즉시실행 함수식(immediately-invoked function expression (IIFE))
    (function() {
      console.log('Welcome to the Internet. Please follow me.');
    })();
    ```

  - (if 및 while 등) 블록 내에서 변수에 함수를 할당하는 대신 함수를 선언하지 마십시오. 브라우저는 허용하지만 (마치 'bad news bears'처럼) 모두 다른 방식으로 해석됩니다.
  - **Note:** ECMA-262에서는`block`은 statements의 목록에 정의되어 있습니다 만, 함수 선언은 statements가 없습니다. [이 문제는 ECMA-262의 설명을 참조하십시오. ](http://www.ecma-international.org/publications/files/ECMA-ST/Ecma-262.pdf#page=97).

    ```javascript
    // bad
    if (currentUser) {
      function test() {
        console.log('Nope.');
      }
    }

    // good
    var test;
    if (currentUser) {
      test = function test() {
        console.log('Yup.');
      };
    }
    ```

  - 매개 변수(parameter)에 `arguments`를 절대 지정하지 마십시오. 이것은 함수 범위로 전달 될`arguments`객체의 참조를 덮어 쓸 것입니다.

    ```javascript
    // bad
    function nope(name, options, arguments) {
      // ...stuff...
    }

    // good
    function yup(name, options, args) {
      // ...stuff...
    }
    ```
