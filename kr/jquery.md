## jQuery

  - jQuery Object의 변수 앞에는 `$`을 부여해 주십시오.

    ```javascript
    // bad
    var sidebar = $('.sidebar');

    // good
    var $sidebar = $('.sidebar');
    ```

  - jQuery 쿼리결과를 캐시해주십시오.

    ```javascript
    // bad
    function setSidebar() {
      $('.sidebar').hide();

      // ...stuff...

      $('.sidebar').css({
        'background-color': 'pink'
      });
    }

    // good
    function setSidebar() {
      var $sidebar = $('.sidebar');
      $sidebar.hide();

      // ...stuff...

      $sidebar.css({
        'background-color': 'pink'
      });
    }
    ```

  - DOM 검색은 Cascading `$('.sidebar ul')` 이나 parent > child `$('.sidebar > ul')` 를 사용해주십시오. [jsPerf](http://jsperf.com/jquery-find-vs-context-sel/16)
  - jQuery Object 검색은 스코프가 붙은 `find`를 사용해주십시오.

    ```javascript
    // bad
    $('ul', '.sidebar').hide();

    // bad
    $('.sidebar').find('ul').hide();

    // good
    $('.sidebar ul').hide();

    // good
    $('.sidebar > ul').hide();

    // good
    $sidebar.find('ul');
    ```

    **[[↑]](#TOC)**

