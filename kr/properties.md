## Properties

  - 속성에 액세스하려면 도트`.`를 사용하십시오.

    ```javascript
    var luke = {
      jedi: true,
      age: 28
    };

    // bad
    var isJedi = luke['jedi'];

    // good
    var isJedi = luke.jedi;
    ```

  - 변수를 사용하여 속성에 접근하려면 대괄호`[]`을 사용하십시오.

    ```javascript
    var luke = {
      jedi: true,
      age: 28
    };

    function getProp(prop) {
      return luke[prop];
    }

    var isJedi = getProp('jedi');
    ```
