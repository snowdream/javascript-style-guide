## Условни изрази и равенства

  - Използвайте `===` и `!==` вместо `==` и `!=`.
  - Условните изрази се проверяват чрез прехвърляне към `ToBoolean` метод и винаги следват тези прости правила:

    + **Objects** става **true**
    + **Undefined** става **false**
    + **Null** става **false**
    + **Booleans** става **the value of the boolean**
    + **Numbers** става **false** Ако **+0, -0, or NaN**, иначе **true**
    + **Strings** става **false** ако е празен низ`''`, иначе **true**

    ```javascript
    if ([0]) {
      // вярно 
      // Ако масивът е обект, обектите връщат true
    }
    ```
 
  - Използвайте кратки варианти.

    ```javascript
    // лоша практика
    if (name !== '') {
      // ...stuff...
    }

    // добра практика
    if (name) {
      // ...stuff...
    }

    // лоша практика
    if (collection.length > 0) {
      // ...stuff...
    }

    // добра практика
    if (collection.length) {
      // ...stuff...
    }
    ```

  - За повече информация [Truth Equality and JavaScript](http://javascriptweblog.wordpress.com/2011/02/07/truth-equality-and-javascript/#more-2108) by Angus Croll
