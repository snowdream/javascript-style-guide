## Коментари

  - Използвайте `/** ... */` за многоредови коментари. Включете обяснение, изяснете типове и стойности за всички параметри и стойности, които се връщат от функцията.

    ```javascript
    // лоша практика
    // make() returns a new element
    // based on the passed in tag name
    //
    // @param <String> tag
    // @return <Element> element
    function make(tag) {

      // ...stuff...

      return element;
    }

    // добра практика
    /**
     * make() returns a new element
     * based on the passed in tag name
     *
     * @param <String> tag
     * @return <Element> element
     */
    function make(tag) {

      // ...stuff...

      return element;
    }
    ```

  - Използвайте `//` за едноредови коментари. Поставятйте едноредовите коментари на нов ред над предмета на обяснение. Поставете и празен ред преди коментара.


    ```javascript
    // лоша практика
    var active = true;  // is current tab

    // добра практика
    // is current tab
    var active = true;

    // лоша практика
    function getType() {
      console.log('fetching type...'); 
      var type = this._type || 'no type';

      return type;
    }

    // добра практика
    function getType() {
      console.log('fetching type...');
 
      var type = this._type || 'no type';

      return type;
    }
    ```

  - Поставяйте в коментарите `FIXME` или `TODO`, помагайки на други разработчици да разберат ако има проблем, който трябва да бъде прегледан или да предлагате решение на проблем.  Това са различни от обикновените коментари, защото 
те предлагат действие. 
`FIXME -- need to figure this out` or `TODO -- need to implement`.

  - Използвайте `// FIXME:` да анонсирате проблем

    ```javascript
    function Calculator() {

      // FIXME: shouldn't use a global here
      total = 0;

      return this;
    }
    ```

  - Използвайте `// TODO:` да анонсирате решение на проблема

    ```javascript
    function Calculator() {

      // TODO: total should be configurable by an options param
      this.total = 0;

      return this;
    }
  ```