## Функции

  - Функции-изрази:

    ```javascript
    // Анонимни функции изрази
    var anonymous = function() {
      return true;
    };

    // Именувание функции изрази
    var named = function named() {
      return true;
    };

    // Моментално изпълнени функции-изрази (IIFE)
    (function() {
      console.log('Welcome to the Internet. Please follow me.');
    })();
    ```

  - Никога не декларирайте функция в нефункционелен блок (if, while, etc). Дайте и стойност и на променлива вместо това. Браузърите ще дадат това, но ще го интерпретират различно, което не е никак добре.
  - **Забележка:** ECMA-262 дефинира `block` като лист с декларации/твърдения. Декларирането на фунция не е твърдение. [Read ECMA-262's note on this issue](http://www.ecma-international.org/publications/files/ECMA-ST/Ecma-262.pdf#page=97).

    ```javascript
    // лоша практика
    if (currentUser) {
      function test() {
        console.log('Nope.');
      }
    }

    // добра практика
    var test;
    if (currentUser) {
      test = function test() {
        console.log('Yup.');
      };
    }
    ```

  - Никога не наименовайте параметър `arguments`, това ще вземе значението на `arguments`-обекта, който е деклариран по подразбиране във всеки скоуп.

    ```javascript
    // лоша практика
    function nope(name, options, arguments) {
      // ...stuff...
    }

    // добра практика
    function yup(name, options, args) {
      // ...stuff...
    }
    ```