##Достъпване

  - Не са необходими функции за променливи
  - Ако има - ползвайте подобни на  getVal() и setVal('hello')

    ```javascript
    // лоша практика
    dragon.age();

    // добра практика
    dragon.getAge();

    // лоша практика
    dragon.age(25);

    // добра практика
    dragon.setAge(25);
    ```

  - Ако променлива е булева стойност, използвайте isVal() или hasVal()

    ```javascript
    // лоша практика
    if (!dragon.age()) {
      return false;
    }

    // добра практика
    if (!dragon.hasAge()) {
      return false;
    }
    ```

  - Може да се създадат get() и set() функции, но бъдете постоянни с тях.

    ```javascript
    function Jedi(options) {
      options || (options = {});
      var lightsaber = options.lightsaber || 'blue';
      this.set('lightsaber', lightsaber);
    }

    Jedi.prototype.set = function(key, val) {
      this[key] = val;
    };

    Jedi.prototype.get = function(key) {
      return this[key];
    };
    ```