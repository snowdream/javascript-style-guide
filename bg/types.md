## Типове

  - **Примитивни**: Когато достъпвате примитивен тип се работи директно със неговата стойност. 

    + `string`
    + `number`
    + `boolean`
    + `null`
    + `undefined`

    ```javascript
    var foo = 1,
        bar = foo;

    bar = 9;

    console.log(foo, bar); // => 1, 9
    ```
  - **Съставни**: Когато достъпвате комплексен тип данни се работи с референция на стойността и. 

    + `object`
    + `array`
    + `function`

    ```javascript
    var foo = [1, 2],
        bar = foo;

    bar[0] = 9;

    console.log(foo[0], bar[0]); // => 9, 9
    ```