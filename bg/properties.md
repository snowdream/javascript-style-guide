## Полета

  - Използвайте '.' за достъпване на полета.

    ```javascript
    var luke = {
      jedi: true,
      age: 28
    };

    // лоша практика
    var isJedi = luke['jedi'];

    // добра практика
    var isJedi = luke.jedi;
    ```

  - Използвайте `[]` когато достъпвате полета с променлива.

    ```javascript
    var luke = {
      jedi: true,
      age: 28
    };

    function getProp(prop) {
      return luke[prop];
    }

    var isJedi = getProp('jedi');
    ```
