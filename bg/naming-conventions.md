##Наименуване практики

  - Избягвайте имена от 1 буква. Обяснявайте какво наименувате.

    ```javascript
    // лоша практика
    function q() {
      // ...stuff...
    }

    // добра практика
    function query() {
      // ..stuff..
    }
    ```

  - Използвайте camelCase, когато именовате обекти, функции, и инстанции

    ```javascript
    // лоша практика
    var OBJEcttsssss = {};
    var this_is_my_object = {};
    function c() {};
    var u = new user({
      name: 'Bob Parr'
    });

    // добра практика
    var thisIsMyObject = {};
    function thisIsMyFunction() {};
    var user = new User({
      name: 'Bob Parr'
    });
    ```

  - Използвайте PascalCase когато именувате конструктори или класове

    ```javascript
    // лоша практика
    function user(options) {
      this.name = options.name;
    }

    var лоша практика = new user({
      name: 'nope'
    });

    // добра практика
    function User(options) {
      this.name = options.name;
    }

    var добра практика = new User({
      name: 'yup'
    });
    ```

  - Използвайте долно тире в началото `_`, когато именувате частни променливи

    ```javascript
    // лоша практика
    this.__firstName__ = 'Panda';
    this.firstName_ = 'Panda';

    // добра практика
    this._firstName = 'Panda';
    ```

  - Когато използвате референции към `this` използвайте `_this`.

    ```javascript
    // лоша практика
    function() {
      var self = this;
      return function() {
        console.log(self);
      };
    }

    // лоша практика
    function() {
      var that = this;
      return function() {
        console.log(that);
      };
    }

    // добра практика
    function() {
      var _this = this;
      return function() {
        console.log(_this);
      };
    }
    ```

  - Именувайте си функцийте. Това е добра практика за проследяване на стака.

    ```javascript
    // лоша практика
    var log = function(msg) {
      console.log(msg);
    };

    // добра практика
    var log = function log(msg) {
      console.log(msg);
    };
    ```