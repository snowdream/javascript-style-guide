## Масиви

  - Използвайте варианта със скобите за създаване на масив

    ```javascript
    // лоша практика
    var items = new Array();

    // добра практика
    var items = [];
    ```

  - Ако не знаете дължината на масив, използвайте Array#push.

    ```javascript
    var someStack = [];


    // лоша практика
    someStack[someStack.length] = 'abracadabra';

    // добра практика
    someStack.push('abracadabra');
    ```

  - Ако имате нужда да копирате масив използвайте Array#slice. [jsPerf](http://jsperf.com/converting-arguments-to-an-array/7)

    ```javascript
    var len = items.length,
        itemsCopy = [],
        i;

    // лоша практика
    for (i = 0; i < len; i++) {
      itemsCopy[i] = items[i];
    }

    // добра практика
    itemsCopy = items.slice();
    ```

  - Да конвертирате подобен на масив обект към масив, използвайте Array#slice.

    ```javascript
    function trigger() {
      var args = Array.prototype.slice.call(arguments);
      ...
    }
    ```
