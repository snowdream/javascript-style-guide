## Променливи

  - Винаги използвайте `var` за деклариране на променливи. В противен случай се декларират глобални променливи. Ние искаме да не 'замърсяване' глобалното пространство с тях. 'Капитан планета ни предупреди за тях'.

    ```javascript
    // лоша практика
    superPower = new SuperPower();

    // добра практика
    var superPower = new SuperPower();
    ```

  - Използвайте една `var` декларация за много променливи и декларирайте всяка променлива на нов ред.

    ```javascript
    // лоша практика
    var items = getItems();
    var goSportsTeam = true;
    var dragonball = 'z';

    // добра практика
    var items = getItems(),
        goSportsTeam = true,
        dragonball = 'z';
    ```

  - Декларирайте променливи, непродобили стойност последни. Това е полезно, когато по-късно се нуждаете от променлива зависеща от някоя от предходно дефинираните променливи.

    ```javascript
    // лоша практика
    var i, len, dragonball,
        items = getItems(),
        goSportsTeam = true;

    // лоша практика
    var i, items = getItems(),
        dragonball,
        goSportsTeam = true,
        len;

    // добра практика
    var items = getItems(),
        goSportsTeam = true,
        dragonball,
        length,
        i;
    ```

  - Слагайте променливите винаги в началото на скоупа им. Това помага да се избегнат проблеми с декларации и неправомерно извикване на недефинирани променливи.

    ```javascript
    // лоша практика
    function() {
      test();
      console.log('doing stuff..');

      //..други..

      var name = getName();

      if (name === 'test') {
        return false;
      }

      return name;
    }

    // добра практика
    function() {
      var name = getName();

      test();
      console.log('doing stuff..');

      //..other stuff..

      if (name === 'test') {
        return false;
      }

      return name;
    }

    // лоша практика
    function() {
      var name = getName();

      if (!arguments.length) {
        return false;
      }

      return true;
    }

    // добра практика
    function() {
      if (!arguments.length) {
        return false;
      }

      var name = getName();

      return true;
    }
    ```