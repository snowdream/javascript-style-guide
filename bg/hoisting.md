## Деклариране и използване на променливи

  - Декларацията на променливи се поставя в началото на функция или обект, тяхното изпозлване става по-надолу.

    ```javascript
    // Това няма да работи (приемаме, че няма 
  // дефинирана notDefined в глобалния скоуп)
    function example() {
      console.log(notDefined); // => throws a ReferenceError
    }

    // Извикване на променлива, преди декларацията и ще работи,
  // но стойносттта и няма да се взима
   
    function example() {
      console.log(declaredButNotAssigned); // => undefined
      var declaredButNotAssigned = true;
    }

  // Интерпретаторът поставя дефинирането в началото
    // на скоупа. Примерът може да се пренапише така :
  
    function example() {
      var declaredButNotAssigned;
      console.log(declaredButNotAssigned); // => undefined
      declaredButNotAssigned = true;
    }
    ```

  - Анонимните функции-изрази поставят най-горе дефиницията на променливи, но не и стойността/фунцкията,
    присвоена на променливата.

    ```javascript
    function example() {
      console.log(anonymous); // => undefined

      anonymous(); // => TypeError anonymous is not a function

      var anonymous = function() {
        console.log('anonymous function expression');
      };
    }
    ```

  - Именуваните функции-изрази прехвърлят името на променливата, а не името на функцията или тялото и.

    ```javascript
    function example() {
      console.log(named); // => undefined

      named(); // => TypeError named is not a function

      superPower(); // => ReferenceError superPower is not defined

      var named = function superPower() {
        console.log('Flying');
      };
    }

  
  // същото е вярно, когато името на функцията
    // е същото като името на променливата.
    function example() {
      console.log(named); // => undefined

      named(); // => TypeError named is not a function

      var named = function named() {
        console.log('named');
      }
    }
    ```

  - Декларацията на функцията прехвърля името и тялото на функция.

    ```javascript
    function example() {
      superPower(); // => Flying

      function superPower() {
        console.log('Flying');
      }
    }
    ```

  - За повече информация [JavaScript Scoping & Hoisting](http://www.adequatelygood.com/2010/2/JavaScript-Scoping-and-Hoisting) от [Ben Cherry](http://www.adequatelygood.com/)
