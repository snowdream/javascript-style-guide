## Празни полета

  - Използвайте табулации с 2 празни полета

    ```javascript
    // лоша практика
    function() {
    ∙∙∙∙var name;
    }

    // лоша практика
    function() {
    ∙var name;
    }

    // добра практика
    function() {
    ∙∙var name;
    }
    ```
  - Поставяйте 1 празно пространство преди начална скоба.

    ```javascript
    // лоша практика
    function test(){
      console.log('test');
    }

    // добра практика
    function test() {
      console.log('test');
    }

    // лоша практика
    dog.set('attr',{
      age: '1 year',
      breed: 'Bernese Mountain Dog'
    });

    // добра практика
    dog.set('attr', {
      age: '1 year',
      breed: 'Bernese Mountain Dog'
    });
    ```
  - Поставяйте празен ред в края на файл.

    ```javascript
    // лоша практика
    (function(global) {
      // ...stuff...
    })(this);
    ```

    ```javascript
    // добра практика
    (function(global) {
      // ...stuff...
    })(this);

    ```

  - Използвайте йерархично подравняване при дълги вериги от методи.

    ```javascript
    // лоша практика
    $('#items').find('.selected').highlight().end().find('.open').updateCount();

    // добра практика
    $('#items')
      .find('.selected')
        .highlight()
        .end()
      .find('.open')
        .updateCount();

    // лоша практика
    var leds = stage.selectAll('.led').data(data).enter().append('svg:svg').class('led', true)
        .attr('width',  (radius + margin) * 2).append('svg:g')
        .attr('transform', 'translate(' + (radius + margin) + ',' + (radius + margin) + ')')
        .call(tron.led);

    // добра практика
    var leds = stage.selectAll('.led')
        .data(data)
      .enter().append('svg:svg')
        .class('led', true)
        .attr('width',  (radius + margin) * 2)
      .append('svg:g')
        .attr('transform', 'translate(' + (radius + margin) + ',' + (radius + margin) + ')')
        .call(tron.led);
    ```
