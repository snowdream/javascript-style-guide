## Точка и запетая

  - **ДА.**

    ```javascript
    // лоша практика
    (function() {
      var name = 'Skywalker'
      return name
    })()

    // добра практика
    (function() {
      var name = 'Skywalker';
      return name;
    })();

    // добра практика
    ;(function() {
      var name = 'Skywalker';
      return name;
    })();
    ```