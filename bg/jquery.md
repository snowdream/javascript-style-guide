## jQuery

  - Поставете jQuery обекти променливи с `$`.

    ```javascript
    // лоша практика
    var sidebar = $('.sidebar');

    // добра практика
    var $sidebar = $('.sidebar');
    ``` 

    ```javascript
    // лоша практика
    function setSidebar() {
      $('.sidebar').hide();

      // ...stuff...

      $('.sidebar').css({
        'background-color': 'pink'
      });
    }

    // добра практика
    function setSidebar() {
      var $sidebar = $('.sidebar');
      $sidebar.hide();

      // ...stuff...

      $sidebar.css({
        'background-color': 'pink'
      });
    }
    ```

  //- For DOM queries use Cascading `$('.sidebar ul')` or parent > child `$('.sidebar > ul')`. [jsPerf](http://jsperf.com/jquery-find-vs-context-sel/16)
  - За DOM търсене използвайте каскадно `$('.sidebar ul')` or parent > child `$('.sidebar > ul')`. [jsPerf](http://jsperf.com/jquery-find-vs-context-sel/16)
  - Use `find` with scoped jQuery object queries.
  - Използвайте `find` в jQuery за по-лесно намиране в скоуп.

    ```javascript
    // лоша практика
    $('ul', '.sidebar').hide();

    // лоша практика
    $('.sidebar').find('ul').hide();

    // добра практика
    $('.sidebar ul').hide();

    // добра практика
    $('.sidebar > ul').hide();

    // добра практика
    $sidebar.find('ul');
    ```
