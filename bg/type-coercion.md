## Преобразуване на типове

  - Изпълнявайте преобразуването на типове в началото.
  - Низ:

    ```javascript
    //  => this.reviewScore = 9;

    // лоша практика
    var totalScore = this.reviewScore + '';

    // добра практика
    var totalScore = '' + this.reviewScore;

    // лоша практика
    var totalScore = '' + this.reviewScore + ' total score';

    // добра практика
    var totalScore = this.reviewScore + ' total score';
    ```

  - Use `parseInt` for Numbers and always with a radix for type casting.

    ```javascript
    var inputValue = '4';

    // лоша практика
    var val = new Number(inputValue);

    // лоша практика
    var val = +inputValue;

    // лоша практика
    var val = inputValue >> 0;

    // лоша практика
    var val = parseInt(inputValue);

    // добра практика
    var val = Number(inputValue);

    // добра практика
    var val = parseInt(inputValue, 10);
    ```

  
  - В случай, че решите да сте палави с  `parseInt` и това ви е ахилесовата пета и е нужно да използате смяна на битове [performance reasons](http://jsperf.com/coercion-vs-casting/3), оставете коментар какво и защо правите.
  
  - **Забележка:** Бъдете предпазливи като използвате промяна на битове. Числата са представени чрез [64-bit values](http://es5.github.io/#x4.3.19), но смяната на битове винаги връща 32-битов интиджер ([source](http://es5.github.io/#x11.7)). Смяната на битове може да доведе до неочаквано поведени при числови стойности на 32 бита. [Discussion](https://github.com/airbnb/javascript/issues/109)

    ```javascript
    // добра практика
    /**
     * parseInt беше причина кода ми да е бавен.
     * Промяната на битове в стринга
     * към числа го направи по-бърз.
     */
    var val = inputValue >> 0;
    ```

  - Булеви стойности:

    ```javascript
    var age = 0;

    // лоша практика
    var hasAge = new Boolean(age);

    // добра практика
    var hasAge = Boolean(age);

    // добра практика
    var hasAge = !!age;
    ```