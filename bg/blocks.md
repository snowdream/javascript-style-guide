##Блокове
  - Използвайте скоби с всички многоредови блокове.

    ```javascript
    // лоша практика
    if (test)
      return false;

    // добра практика
    if (test) return false;

    // добра практика
    if (test) {
      return false;
    }

    // лоша практика
    function() { return false; }

    // добра практика
    function() {
      return false;
    }
    ```