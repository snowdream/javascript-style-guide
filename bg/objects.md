## Обекти
  - Използвайте втория вариант за създаване на обект.

    ```javascript
    // лоша практика
    var item = new Object();

    // добра практика
    var item = {};
    ```

  - Не използвайте [запазени думи](http://es5.github.io/#x7.6.1) за деклариране на променливи. Тези променливи няма да работят на IE8. [Повече](https://github.com/airbnb/javascript/issues/61)

    ```javascript
    // лоша практика
    var superman = {
      default: { clark: 'kent' },
      private: true
    };

    // добра практика
    var superman = {
      defaults: { clark: 'kent' },
      hidden: true
    };
    ```

  - Използвайте смислени синоними на мястото на запазени думи.

    ```javascript
    // лоша практика
    var superman = {
      class: 'alien'
    };

    // лоша практика
    var superman = {
      klass: 'alien'
    };

    // добра практика
    var superman = {
      type: 'alien'
    };
    ```