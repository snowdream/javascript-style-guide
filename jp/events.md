## イベント

  - （DOMイベントやBackbone eventsのような独自の）イベントへペイロードの値を渡す場合は、生の値の代わりにハッシュ引数を渡してください。
こうすることで、後の開発者がイベントに関連する全てのハンドラを見つけて更新することなく、イベント・ぺイロードに値を追加することが出来ます。例えば、これの代わりに:

    ```js
    // bad
    $(this).trigger('listingUpdated', listing.id);

    ...

    $(this).on('listingUpdated', function(e, listingId) {
      // do something with listingId
    });
    ```

    こちらの方が好まれます。:

    ```js
    // good
    $(this).trigger('listingUpdated', { listingId : listing.id });

    ...

    $(this).on('listingUpdated', function(e, data) {
    // do something with data.listingId
    });
    ```