##関数

  - 関数式

    ```javascript
    // 無名関数
    var anonymous = function() {
      return true;
    };

    // 名前付き関数
    var named = function named() {
      return true;
    };

    // 即時関数
    (function() {
      console.log('Welcome to the Internet. Please follow me.');
    })();
    ```

  - （ifやwhileなど）ブロック内で、変数に関数を代入する代わりに関数を宣言しないでください。ブラウザはそのことを許可しますが、（それはまるで「頑張れベアーズ」の悪ガキ達のように）すべて違ったように解釈されます。
  - **注意:** ECMA-262 では `block` はstatementsの一覧に定義されていますが、関数宣言はstatementsではありません。[この問題についてはECMA-262の記述を参照してください。](http://www.ecma-international.org/publications/files/ECMA-ST/Ecma-262.pdf#page=97).

    ```javascript
    // bad
    if (currentUser) {
      function test() {
        console.log('Nope.');
      }
    }

    // good
    var test;
    if (currentUser) {
      test = function test() {
        console.log('Yup.');
      };
    }
    ```

  - パラメータに `arguments` を指定しないでください。これは、関数スコープに渡される `arguments` オブジェクトの参照を上書きしてしまうためです。

    ```javascript
    // bad
    function nope(name, options, arguments) {
      // ...stuff...
    }

    // good
    function yup(name, options, args) {
      // ...stuff...
    }
    ```