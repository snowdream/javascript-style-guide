## コメント

  - 複数行のコメントは`/** ... */` を使用してください。その中には説明とすべてのパラメータと戻り値についての型や値を記述してください。

    ```javascript
    // bad
    // make() returns a new element
    // based on the passed in tag name
    //
    // @param  tag
    // @return  element
    function make(tag) {

      // ...stuff...

      return element;
    }

    // good
    /**
     * make() returns a new element
     * based on the passed in tag name
     *
     * @param  tag
     * @return  element
     */
    function make(tag) {

      // ...stuff...

      return element;
    }
    ```

  - 単一行コメントには`//` を使用してください。コメントを加えたいコードの上部に配置してください。また、コメントの前に空行を入れてください。

    ```javascript
    // bad
    var active = true;  // is current tab

    // good
    // is current tab
    var active = true;

    // bad
    function getType() {
      console.log('fetching type...');
      // set the default type to 'no type'
      var type = this._type || 'no type';

      return type;
    }

    // good
    function getType() {
      console.log('fetching type...');

      // set the default type to 'no type'
      var type = this._type || 'no type';

      return type;
    }
    ```

  - 問題を指摘して再考を促す場合や、問題の解決策を提案する場合など、コメントの前に `FIXME` や `TODO` を付けることで他のデベロッパの素早い理解を助けることができます。これらは、何らかのアクションを伴うという意味で通常のコメントとは異なります。アクションとは `FIXME -- 解決策が必要` もしくは `TODO -- 実装が必要` です。


  - 問題に対する注釈として `// FIXME:` を使用してください。

    ```javascript
    function Calculator() {

      // FIXME: グローバル変数を使用するべきではない。
      total = 0;

      return this;
    }
    ```

  - 問題の解決策に対する注釈として `// TODO:` を使用してください。

    ```javascript
    function Calculator() {

      // TODO: total はオプションパラメータとして設定されるべき。
      this.total = 0;
      return this;
    }
  ```