##オブジェクト

  - オブジェクトを作成する際は、リテラル構文を使用してください。

    ```javascript
    // bad
    var item = new Object();

    // good
    var item = {};
    ```

  - [予約語](http://es5.github.io/#x7.6.1)をキーとして使用しないでください。これはIE8で動作しません。参照→[Issue](https://github.com/airbnb/javascript/issues/61)

    ```javascript
    // bad
    var superman = {
      default: { clark: 'kent' },
      private: true
    };

    // good
    var superman = {
      defaults: { clark: 'kent' },
      hidden: true
    };
    ````
    
  - 予約語の代わりに分かりやすい同義語を使用してください。

    ```javascript
    // bad
    var superman = {
      class: 'alien'
    };

    // bad
    var superman = {
      klass: 'alien'
    };

    // good
    var superman = {
      type: 'alien'
    };

    ```
    **[[⬆ ページのTopへ戻る]](#TOC)**

## <a name='arrays'>配列</a> [原文](https://github.com/airbnb/javascript#arrays)

  - 配列を作成する際は、リテラル構文を使用してください。

    ```javascript
    // bad
    var items = new Array();

    // good
    var items = [];
    ```

  - 長さが不明な場合はArray#pushを使用してください。
  
    ```javascript
    var someStack = [];


    // bad
    someStack[someStack.length] = 'abracadabra';

    // good
    someStack.push('abracadabra');
    ```

  - 配列をコピーする必要がある場合、Array#sliceを使用してください。参考（英語）→[jsPerf](http://jsperf.com/converting-arguments-to-an-array/7)

    ```javascript
    var len = items.length,
        itemsCopy = [],
        i;

    // bad
    for (i = 0; i < len; i++) {
      itemsCopy[i] = items[i];
    }

    // good
    itemsCopy = items.slice();
    ```

  - Allay-LikeなオブジェクトをArrayに変換する場合は、Array#sliceを使用してください。
  
    ```javascript
    function trigger() {
      var args = Array.prototype.slice.call(arguments);
      ...
    }
   ```