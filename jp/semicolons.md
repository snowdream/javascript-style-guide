## セミコロン

  - **もちろん使いましょう。**

    ```javascript
    // bad
    (function() {
      var name = 'Skywalker'
      return name
    })()

    // good
    (function() {
      var name = 'Skywalker';
      return name;
    })();

    // good(即時関数を伴う2つのファイルを連結した場合に、引数となる部分を保護します。)
    ;(function() {
      var name = 'Skywalker';
      return name;
    })();
    ```
    [詳細](http://stackoverflow.com/a/7365214/1712802)