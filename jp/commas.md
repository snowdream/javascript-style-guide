## カンマ

  - 先頭のカンマは **やめてください。**

    ```javascript
    // bad
    var once
      , upon
      , aTime;

    // good
    var once,
        upon,
        aTime;

    // bad
    var hero = {
        firstName: 'Bob'
      , lastName: 'Parr'
      , heroName: 'Mr. Incredible'
      , superPower: 'strength'
    };

    // good
    var hero = {
      firstName: 'Bob',
      lastName: 'Parr',
      heroName: 'Mr. Incredible',
      superPower: 'strength'
    };
    ```

　- 末尾の余計なカンマも **やめてください。** これはIE6/7とquirksmodeのIE9で問題を引き起こす可能性があります。
  さらに、ES3のいくつかの実装において、余計なカンマがある場合、配列に長さを追加します。
  これは、ES5の中で明らかにされました。([参考](http://es5.github.io/#D)):

  > 第5版では、末尾の余計なカンマが存在するArrayInitialiser（配列初期化演算子）であっても、配列に長さを追加しないという事実を明確にしています。
これは第3版から意味的な変更ではありませんが、いくつかの実装は以前よりこれを誤解していたかもしれません。

  ```javascript
    // bad
    var hero = {
      firstName: 'Kevin',
      lastName: 'Flynn',
    };

    var heroes = [
      'Batman',
      'Superman',
    ];

    // good
    var hero = {
      firstName: 'Kevin',
      lastName: 'Flynn'
    };

    var heroes = [
      'Batman',
      'Superman'
    ];
  ```