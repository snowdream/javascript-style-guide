## 型変換と強制

  - 文の先頭で型の強制を行います。
  - 文字列

    ```javascript
    //  => this.reviewScore = 9;

    // bad
    var totalScore = this.reviewScore + '';

    // good
    var totalScore = '' + this.reviewScore;

    // bad
    var totalScore = '' + this.reviewScore + ' total score';

    // good
    var totalScore = this.reviewScore + ' total score';
    ```

  - 数値には`parseInt` を使用してください。常に型変換のための基数を引数に渡してください。
 
    ```javascript
    var inputValue = '4';

    // bad
    var val = new Number(inputValue);

    // bad
    var val = +inputValue;

    // bad
    var val = inputValue >> 0;

    // bad
    var val = parseInt(inputValue);

    // good
    var val = Number(inputValue);

    // good
    var val = parseInt(inputValue, 10);
    ````

  - 何らかの理由により `parseInt` がボトルネックとなっており、[パフォーマンス的な理由](http://jsperf.com/coercion-vs-casting/3)でビットシフトを使用す必要がある場合、
  やろうとしている事について、why（なぜ）とwhat（何を）の説明をコメントとして残してください。

    ```javascript
    // good
    /**
     * parseIntがボトルネックとなっていたため、
     * ビットシフトで文字列を数値へ強制的に変換することで
     * パフォーマンスを改善させます。
     */
    var val = inputValue >> 0;
    ```

  - **注意:** ビットシフトを使用する場合の注意事項。数値は[64ビット倍精度](http://es5.github.io/#x4.3.19)として表現されていますが、ビットシフト演算した場合は常に32ビット単精度で返されます([エビデンス](http://es5.github.io/#x11.7))。
  32ビット以上の数値をビットシフトする場合、予期せぬ振る舞いを起こす可能性があります([議論](https://github.com/airbnb/javascript/issues/109))。符号付き32ビット整数の最大値は2,147,483,647です。

    ```javascript
    2147483647 >> 0 //=> 2147483647
    2147483648 >> 0 //=> -2147483648
    2147483649 >> 0 //=> -2147483647
    ```

  - 真偽値

    ```javascript
    var age = 0;

    // bad
    var hasAge = new Boolean(age);

    // good
    var hasAge = Boolean(age);

    // good
    var hasAge = !!age;
    ```