## ライセンス

MITライセンス
 
著作権(c)　2014 Airbnb  
翻訳　2014 mitsuruog

このソフトウェアおよび関連する文書ファイル（以下「本ソフトウェア」という。）の複製物を取得するあらゆる者に対し、
以下の条件にしたがって本ソフトウェアを制限なしに扱うことを無償で許諾する。
そこには、本ソフトウェアの複製を使用し、複製し、改変し、結合し、公表し、頒布し、サブライセンスし、
および/または販売する権利、また、本ソフトウェアを与えられた者に上記のようにすることを許諾する権利を含むがそれらに限られない。

上記の著作権表示および本許諾表示は「本ソフトウェア」のすべての複製物または重要部分の中に含めなければならない。

「本ソフトウェア」は「現状のまま」で提供され、明示または黙示を問わず、
商品性、特定目的への適合性および非侵害を含むがそれに限られない、あらゆる種類の保証も伴わないものとする。
著作者または著作権者は、契約、不法行為またはその他の行為であるかにかかわらず、
ソフトウェアまたはソフトウェアの使用もしくはその他の取り扱いから、またはそれらに関連して生じた、
いかなるクレーム、損害賠償その他の責任を負わない。