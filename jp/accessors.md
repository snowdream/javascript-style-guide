## アクセサ（Accessors）

  - プロパティのためのアクセサ（Accessor）関数は必須ではありません。
  - アクセサ関数が必要な場合、`getVal()` や `setVal('hello')` としてください。

    ```javascript
    // bad
    dragon.age();

    // good
    dragon.getAge();

    // bad
    dragon.age(25);

    // good
    dragon.setAge(25);
    ```

  - プロパティが真偽値の場合、`isVal()` や`hasVal()` としてください。

    ```javascript
    // bad
    if (!dragon.age()) {
      return false;
    }
  
    // good
    if (!dragon.hasAge()) {
      return false;
    }
    ```

  - 一貫していれば、`get()` や`set()` という関数を作成することも可能です。

    ```javascript
    function Jedi(options) {
      options || (options = {});
      var lightsaber = options.lightsaber || 'blue';
      this.set('lightsaber', lightsaber);
    }

    Jedi.prototype.set = function(key, val) {
      this[key] = val;
    };

    Jedi.prototype.get = function(key) {
      return this[key];
    };
    ```