## Точки с запятой

  - **Да.**

    ```javascript
    // плохо
    (function() {
      var name = 'Skywalker'
      return name
    })()

    // хорошо
    (function() {
      var name = 'Skywalker';
      return name;
    })();

    // хорошо
    ;(function() {
      var name = 'Skywalker';
      return name;
    })();
    ```
