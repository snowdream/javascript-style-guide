## Объекты

  - Для создания объекта используйте фигурные скобки. Не создавайте объекты через конструктор `new Object`.

    ```javascript
    // плохо
    var item = new Object();

    // хорошо
    var item = {};
    ```

  - Не используйте [зарезервированные слова](http://es5.github.io/#x7.6.1) в качестве ключей объектов. Они не будут работать в IE8. [Подробнее](https://github.com/airbnb/javascript/issues/61)

    ```javascript
    // плохо
    var superman = {
      default: { clark: 'kent' },
      private: true
    };

    // хорошо
    var superman = {
      defaults: { clark: 'kent' },
      hidden: true
    };
    ```

  - Не используйте ключевые слова (в том числе измененные). Вместо них используйте синонимы.

    ```javascript
    // плохо
    var superman = {
      class: 'alien'
    };

    // плохо
    var superman = {
      klass: 'alien'
    };

    // хорошо
    var superman = {
      type: 'alien'
    };
    ```