## Свойства

  - Используйте точечную нотацию для доступа к свойствам и методам.

    ```javascript
    var luke = {
      jedi: true,
      age: 28
    };

    // плохо
    var isJedi = luke['jedi'];

    // хорошо
    var isJedi = luke.jedi;
    ```

  - Используйте нотацию с `[]`, когда вы получаете свойство, имя для которого хранится в переменной.

    ```javascript
    var luke = {
      jedi: true,
      age: 28
    };

    function getProp(prop) {
      return luke[prop];
    }

    var isJedi = getProp('jedi');
    ```