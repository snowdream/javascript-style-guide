## jQuery

  - Для jQuery-переменных используйте префикс `$`.

    ```javascript
    // плохо
    var sidebar = $('.sidebar');

    // хорошо
    var $sidebar = $('.sidebar');
    ```

  - Кэшируйте jQuery-запросы. Каждый новый jQuery-запрос делает повторный поиск по DOM-дереву, и приложение начинает работать медленнее.

    ```javascript
    // плохо
    function setSidebar() {
      $('.sidebar').hide();

      // ...код...

      $('.sidebar').css({
        'background-color': 'pink'
      });
    }

    // хорошо
    function setSidebar() {
      var $sidebar = $('.sidebar');
      $sidebar.hide();

      // ...код...

      $sidebar.css({
        'background-color': 'pink'
      });
    }
    ```

  - Для DOM-запросов используйте классический каскадный CSS-синтаксис `$('.sidebar ul')` или родитель > потомок `$('.sidebar > ul')`. [jsPerf](http://jsperf.com/jquery-find-vs-context-sel/16)
  - Используйте `find` для поиска внутри DOM-объекта.

    ```javascript
    // плохо
    $('ul', '.sidebar').hide();

    // плохо
    $('.sidebar').find('ul').hide();

    // хорошо
    $('.sidebar ul').hide();

    // хорошо
    $('.sidebar > ul').hide();

    // хорошо
    $sidebar.find('ul');
    ```
