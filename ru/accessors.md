## Геттеры и сеттеры: функции для доступа к значениям объекта

  - Функции универсального доступа к свойствам не требуются
  - Если вам необходимо создать функцию для доступа к переменной, используйте раздельные функции getVal() и setVal('hello')

    ```javascript
    // плохо
    dragon.age();

    // хорошо
    dragon.getAge();

    // плохо
    dragon.age(25);

    // хорошо
    dragon.setAge(25);
    ```

  - Если свойство является логическим(boolean), используйте isVal() или hasVal()

    ```javascript
    // плохо
    if (!dragon.age()) {
      return false;
    }

    // хорошо
    if (!dragon.hasAge()) {
      return false;
    }
    ```

  - Вы можете создавать функции get() и set(), но будьте логичны и последовательны – то есть не добавляйте свойства, которые не могут быть изменены через эти функции.

    ```javascript
    function Jedi(options) {
      options || (options = {});
      var lightsaber = options.lightsaber || 'blue';
      this.set('lightsaber', lightsaber);
    }

    Jedi.prototype.set = function(key, val) {
      this[key] = val;
    };

    Jedi.prototype.get = function(key) {
      return this[key];
    };
    ```