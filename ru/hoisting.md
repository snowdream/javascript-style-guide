## Области видимости

  - Объявление переменных ограничивается областью видимости, а присвоение — нет.

    ```javascript
    // Мы знаем, что это не будет работать
    // если нет глобальной переменной notDefined
    function example() {
      console.log(notDefined); // => выбрасывает код с ошибкой ReferenceError
    }

    // Декларирование переменной после ссылки на нее
    // не будет работать из-за ограничения области видимости.
    function example() {
      console.log(declaredButNotAssigned); // => undefined
      var declaredButNotAssigned = true;
    }

    // Интерпретатор переносит объявление переменной
    // к верху области видимости.
    // Что значит, что предыдущий пример в действительности
    // будет воспринят интерпретатором так:
    function example() {
      var declaredButNotAssigned;
      console.log(declaredButNotAssigned); // => undefined
      declaredButNotAssigned = true;
    }
    ```

  - Объявление анонимной функции поднимает наверх области видимости саму переменную, но не ее значение.

    ```javascript
    function example() {
      console.log(anonymous); // => undefined

      anonymous(); // => TypeError anonymous is not a function
      // Ошибка типов: переменная anonymous не является функцией и не может быть вызвана

      var anonymous = function() {
        console.log('анонимная функция');
      };
    }
    ```

  - Именованные функции поднимают наверх области видимости переменную, не ее значение. Имя функции при этом недоступно в области видимости переменной и доступно только изнутри.

    ```javascript
    function example() {
      console.log(named); // => undefined

      named(); // => TypeError named is not a function
      // Ошибка типов: переменная named не является функцией и не может быть вызвана

      superPower(); // => ReferenceError superPower is not defined (Ошибка ссылки: переменная superPower не найдена в этой области видимости)

      var named = function superPower() {
        console.log('Я лечууууу');
      };
    }

    // То же самое происходит, когда имя функции и имя переменной совпадают.
    // var named доступно изнутри области видимости функции example.
    // function named доступна только изнутри ее самой.
    function example() {
      console.log(named); // => undefined

      named(); // => TypeError named is not a function
      // Ошибка типов: переменная named не является функцией и не может быть вызвана

      var named = function named() {
        console.log('именованная функция');
      }
    }
    ```

  - Объявления функции поднимают на верх текущей области видимости и имя, и свое значение.

    ```javascript
    function example() {
      superPower(); // => Я лечууууу

      function superPower() {
        console.log('Я лечууууу');
      }
    }
    ```

  - Более подробно можно прочитать в статье [JavaScript Scoping & Hoisting](http://www.adequatelygood.com/2010/2/JavaScript-Scoping-and-Hoisting) от [Ben Cherry](http://www.adequatelygood.com/)
