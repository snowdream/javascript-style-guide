## Блоки кода

  - Используйте фигурные скобки для всех многострочных блоков.

    ```javascript
    // плохо
    if (test)
      return false;

    // хорошо
    if (test) return false;

    // хорошо
    if (test) {
      return false;
    }

    // плохо
    function() { return false; }

    // хорошо
    function() {
      return false;
    }
    ```