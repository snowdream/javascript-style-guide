##Приведение типов

  - Выполняйте приведение типов в начале операции, но не делайте его избыточным.
  - Строки:

    ```javascript
    //  => this.reviewScore = 9;

    // плохо
    var totalScore = this.reviewScore + '';

    // хорошо
    var totalScore = '' + this.reviewScore;

    // плохо
    var totalScore = '' + this.reviewScore + ' итого';

    // хорошо
    var totalScore = this.reviewScore + ' итого';
    ```

  - Используйте `parseInt` для чисел и всегда указывайте основание для приведения типов.

    ```javascript
    var inputValue = '4';

    // плохо
    var val = new Number(inputValue);

    // плохо
    var val = +inputValue;

    // плохо
    var val = inputValue >> 0;

    // плохо
    var val = parseInt(inputValue);

    // хорошо
    var val = Number(inputValue);

    // хорошо
    var val = parseInt(inputValue, 10);
    ```

  - Если по какой-либо причине вы делаете что-то дикое, и именно на `parseInt` тратится больше всего ресурсов, используйте побитовый сдвиг [из соображений быстродействия](http://jsperf.com/coercion-vs-casting/3), но обязательно оставьте комментарий с объяснением причин.

    ```javascript
    // хорошо
    /**
     * этот код медленно работал из-за parseInt
     * побитовый сдвиг строки для приведения ее к числу
     * работает значительно быстрее.
     */
    var val = inputValue >> 0;
    ```

  - **Примечание:** Будьте осторожны с побитовыми операциями. Числа в JavaScript являются [64-битными значениями](http://es5.github.io/#x4.3.19), но побитовые операции всегда возвращают 32-битные значенения. [Источник](http://es5.github.io/#x11.7). Побитовые операции над числами, значение которых выходит за 32 бита (верхний предел: 2,147,483,647).

    ```
    2147483647 >> 0 //=> 2147483647
    2147483648 >> 0 //=> -2147483648
    2147483649 >> 0 //=> -2147483647
    ```

  - логические типы(Boolean):

    ```javascript
    var age = 0;

    // плохо
    var hasAge = new Boolean(age);

    // хорошо
    var hasAge = Boolean(age);

    // хорошо
    var hasAge = !!age;
    ```