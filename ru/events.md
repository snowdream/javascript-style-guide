## События

  - Подключая набор данных к событиям (как DOM-событиям, так и js-событиям, например, в Backbone), передавайте объект вместо простой переменной. Это позволяет в процессе всплытия событий добавлять к данному объекту дополнительную информацию.

    ```js
    // плохо
    $(this).trigger('listingUpdated', listing.id);

    ...

    $(this).on('listingUpdated', function(e, listing) {
         //делаем что-нибудь с listing, например:
         listing.name = listings[listing.id]
    });
    ```

    prefer:

    ```js
    // хорошо
    $(this).trigger('listingUpdated', { listingId : listing.id });

    ...

    $(this).on('listingUpdated', function(e, data) {
      // делаем что-нибудь с data.listingId
    });
    ```