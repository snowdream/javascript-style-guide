## Соглашение об именовании

  - Избегайте однобуквенных имен функций. Имена должны давать представление о том, что делает эта функция.

    ```javascript
    // плохо
    function q() {
      // ...код...
    }

    // хорошо
    function query() {
      // ...код...
    }
    ```

  - Используйте camelCase для именования объектов, функций и переменных.

    ```javascript
    // плохо
    var OBJEcttsssss = {};
    var this_is_my_object = {};
    function c() {};
    var u = new user({
      name: 'Bob Parr'
    });

    // хорошо
    var thisIsMyObject = {};
    function thisIsMyFunction() {};
    var user = new User({
      name: 'Bob Parr'
    });
    ```

  - Используйте PascalCase для именования конструкторов классов

    ```javascript
    // плохо
    function user(options) {
      this.name = options.name;
    }

    var bad = new user({
      name: 'Плохиш'
    });

    // хорошо
    function User(options) {
      this.name = options.name;
    }

    var good = new User({
      name: 'Кибальчиш'
    });
    ```

  - Используйте подчеркивание `_` в качестве префикса для именования внутренних методов и переменных объекта.

    ```javascript
    // плохо
    this.__firstName__ = 'Panda';
    this.firstName_ = 'Panda';

    // хорошо
    this._firstName = 'Panda';
    ```

  - Создавая ссылку на `this`, используйте `_this`.

    ```javascript
    // плохо
    function() {
      var self = this;
      return function() {
        console.log(self);
      };
    }

    // плохо
    function() {
      var that = this;
      return function() {
        console.log(that);
      };
    }

    // хорошо
    function() {
      var _this = this;
      return function() {
        console.log(_this);
      };
    }
    ```

  - Задавайте имена для функций. Это повышает читаемость сообщений об ошибках кода.

    ```javascript
    // плохо
    var log = function(msg) {
      console.log(msg);
    };

    // хорошо
    var log = function log(msg) {
      console.log(msg);
    };
    ```