## Строки

  - Используйте одинарные кавычки `''` для строк.

    ```javascript
    // плохо
    var name = "Боб Дилан";

    // хорошо
    var name = 'Боб Дилан';

    // плохо
    var fullName = "Боб " + this.lastName;

    // хорошо
    var fullName = 'Дилан ' + this.lastName;
    ```

  - Строки длиннее 80 символов нужно разделять, выполняя перенос через конкатенацию строк.
  - Осторожно: строки с большим количеством конкатенаций могут отрицательно влиять на быстродействие. [jsPerf](http://jsperf.com/ya-string-concat) и [Обсуждение](https://github.com/airbnb/javascript/issues/40)

    ```javascript
    // плохо
    var errorMessage = 'Эта сверхдлинная ошибка возникла из-за белой обезъяны. Не говори про обезъяну! Не слушай об обезьяне! Не думай об обезъяне!';

    // плохо
    var errorMessage = 'Эта сверхдлинная ошибка возникла из-за белой обезъяны. \
    Не говори про обезъяну! Не слушай об обезьяне! \
    Не думай об обезъяне!';

    // хорошо
    var errorMessage = 'Эта сверхдлинная ошибка возникла из-за белой обезъяны. ' +
      'Не говори про обезъяну! Не слушай об обезьяне! ' +
      'Не думай об обезъяне!';
    ```

  - Когда строка создается программным путем, используйте Array::join вместо объединения строк. В основном для IE: [jsPerf](http://jsperf.com/string-vs-array-concat/2).

    ```javascript
    var items,
        messages,
        length,
        i;

    messages = [{
        state: 'success',
        message: 'Это работает.'
    },{
        state: 'success',
        message: 'Это тоже.'
    },{
        state: 'error',
        message: 'А я томат.'
    }];

    length = messages.length;

    // плохо
    function inbox(messages) {
      items = '<ul>';

      for (i = 0; i < length; i++) {
        items += '<li>' + messages[i].message + '</li>';
      }

      return items + '</ul>';
    }

    // хорошо
    function inbox(messages) {
      items = [];

      for (i = 0; i < length; i++) {
        items[i] = messages[i].message;
      }

      return '<ul><li>' + items.join('</li><li>') + '</li></ul>';
    }
    ```