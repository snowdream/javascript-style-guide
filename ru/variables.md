## Переменные

  - Всегда используйте `var` для объявления переменных. В противном случае переменная будет объявлена глобальной. Загрязнение глобального пространства имен — всегда плохо.

    ```javascript
    // плохо
    superPower = new SuperPower();

    // хорошо
    var superPower = new SuperPower();
    ```

  - Используйте одно `var` объявление переменных для всех переменных, и объявляйте каждую переменную на новой строке.

    ```javascript
    // плохо
    var items = getItems();
    var goSportsTeam = true;
    var dragonball = 'z';

    // хорошо
    var items = getItems(),
        goSportsTeam = true,
        dragonball = 'z';
    ```

  - Объявляйте переменные, которым не присваивается значение, в конце. Это удобно, когда вам необходимо задать значение одной из этих переменных на базе уже присвоенных значений.

    ```javascript
    // плохо
    var i, len, dragonball,
        items = getItems(),
        goSportsTeam = true;

    // плохо
    var i, items = getItems(),
        dragonball,
        goSportsTeam = true,
        len;

    // хорошо
    var items = getItems(),
        goSportsTeam = true,
        dragonball,
        length,
        i;
    ```

  - Присваивайте переменные в начале области видимости. Это помогает избегать проблем с объявлением переменных и областями видимости.

    ```javascript
    // плохо
    function() {
      test();
      console.log('делаю что-нибудь..');

      //..или не делаю...

      var name = getName();

      if (name === 'test') {
        return false;
      }

      return name;
    }

    // хорошо
    function() {
      var name = getName();

      test();
      console.log('делаю что-то полезное..');

      //..продолжаю приносить пользу людям..

      if (name === 'test') {
        return false;
      }

      return name;
    }

    // плохо
    function() {
      var name = getName();

      if (!arguments.length) {
        return false;
      }

      return true;
    }

    // хорошо
    function() {
      if (!arguments.length) {
        return false;
      }

      var name = getName();

      return true;
    }
    ```