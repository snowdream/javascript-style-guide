## Массивы

  - Для создания массива используйте квадратные скобки. Не создавайте массивы через конструктор `new Array`.

    ```javascript
    // плохо
    var items = new Array();

    // хорошо
    var items = [];
    ```

  - Если вы не знаете длину массива, используйте Array::push.

    ```javascript
    var someStack = [];


    // плохо
    someStack[someStack.length] = 'abracadabra';

    // хорошо
    someStack.push('abracadabra');
    ```

  - Если вам необходимо скопировать массив, используйте Array::slice. [jsPerf](http://jsperf.com/converting-arguments-to-an-array/7)

    ```javascript
    var len = items.length,
        itemsCopy = [],
        i;

    // плохо
    for (i = 0; i < len; i++) {
      itemsCopy[i] = items[i];
    }

    // хорошо
    itemsCopy = items.slice();
    ```

  - Чтобы скопировать похожий по свойствам на массив объект (например, NodeList или Arguments), используйте Array::slice.

    ```javascript
    function trigger() {
      var args = Array.prototype.slice.call(arguments);
      ...
    }
    ```
