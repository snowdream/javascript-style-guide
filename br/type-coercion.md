## Casting & Coerção de tipos

  - Faça coerção de tipos no inicio da expressão.
  - Strings:

    ```javascript
    //  => this.reviewScore = 9;

    // ruim
    var totalScore = this.reviewScore + '';

    // bom
    var totalScore = '' + this.reviewScore;

    // ruim
    var totalScore = '' + this.reviewScore + ' total score';

    // bom
    var totalScore = this.reviewScore + ' total score';
    ```

  - Use `parseInt` para Numbers e sempre informe a base de conversão.
  - Se por alguma razão voce está fazendo algo muito underground e o `parseInt` é o gargalo, se usar deslocamento de bits (`Bitshift`) por [questões de performance](http://jsperf.com/coercion-vs-casting/3), deixe um comentário explicando por que voce está fazendo isso.

    ```javascript
    var inputValue = '4';

    // ruim
    var val = new Number(inputValue);

    // ruim
    var val = +inputValue;

    // ruim
    var val = inputValue >> 0;

    // ruim
    var val = parseInt(inputValue);

    // bom
    var val = Number(inputValue);

    // bom
    var val = parseInt(inputValue, 10);

    // bom
    /**
     * parseInt era a razão do código ser lento.
     * Deslocando bits a String faz coerção para Number
     * muito mais rápido.
     */
    var val = inputValue >> 0;
    ```

  - Booleans:

    ```javascript
    var age = 0;

    // ruim
    var hasAge = new Boolean(age);

    // bom
    var hasAge = Boolean(age);

    // bom
    var hasAge = !!age;
    ```
