## Ponto e vírgula

  - **Yup.**

    ```javascript
    // ruim
    (function() {
      var name = 'Skywalker'
      return name
    })()

    // bom
    (function() {
      var name = 'Skywalker';
      return name;
    })();

    // bom
    ;(function() {
      var name = 'Skywalker';
      return name;
    })();
    ```
