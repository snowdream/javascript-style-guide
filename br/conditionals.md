## Expressões Condicionais & Comparações

  - Use `===` e `!==` ao invés de `==` e `!=`.
  - Expressões condicionais são interpretadas usando coerção de tipos e seguem as seguintes regras:

    + **Objeto** equivale a **true**
    + **Undefined** equivale a **false**
    + **Null** equivale a **false**
    + **Booleans** equivalem a **o valor do boolean**
    + **Numbers** equivalem a **false** se **+0, -0, or NaN**, senão **true**
    + **Strings** equivalem a **false** se são vazias `''`, senão **true**

    ```javascript
    if ([0]) {
      // true
      // Um array é um objeto, objetos equivalem a `true`.
    }
    ```

 - Use atalhos.

    ```javascript
    // ruim
    if (name !== '') {
      // ...stuff...
    }

    // bom
    if (name) {
      // ...stuff...
    }

    // ruim
    if (collection.length > 0) {
      // ...stuff...
    }

    // bom
    if (collection.length) {
      // ...stuff...
    }
    ```

  - Para mais informações veja [Truth Equality and JavaScript](http://javascriptweblog.wordpress.com/2011/02/07/truth-equality-and-javascript/#more-2108) por Angus Croll
