## Leading Commas

  - **Nope.**

    ```javascript
    // ruim
    var once
      , upon
      , aTime;

    // bom
    var once,
        upon,
        aTime;

    // ruim
    var hero = {
        firstName: 'Bob'
      , lastName: 'Parr'
      , heroName: 'Mr. Incredible'
      , superPower: 'strength'
    };

    // bom
    var hero = {
      firstName: 'Bob',
      lastName: 'Parr',
      heroName: 'Mr. Incredible',
      superPower: 'strength'
    };
    ```