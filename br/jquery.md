## jQuery

  - Nomeie objetos jQuery com o prefixo `$`.

    ```javascript
    // ruim
    var sidebar = $('.sidebar');

    // bom
    var $sidebar = $('.sidebar');
    ```

  - Guarde as consultas jQuery para reuso.

    ```javascript
    // ruim
    function setSidebar() {
      $('.sidebar').hide();

      // ...stuff...

      $('.sidebar').css({
        'background-color': 'pink'
      });
    }

    // bom
    function setSidebar() {
      var $sidebar = $('.sidebar');
      $sidebar.hide();

      // ...stuff...

      $sidebar.css({
        'background-color': 'pink'
      });
    }
    ```

  - Para pesquisas no DOM use o modo Cascata `$('.sidebar ul')` ou pai > filho `$('.sidebar > ul')`. [jsPerf](http://jsperf.com/jquery-find-vs-context-sel/16)
  - Use `find` em objetos jQuery que estão armazenados em variáveis.

    ```javascript
    // ruim
    $('.sidebar', 'ul').hide();

    // ruim
    $('.sidebar').find('ul').hide();

    // bom
    $('.sidebar ul').hide();

    // bom
    $('.sidebar > ul').hide();

    // bom (slower)
    $sidebar.find('ul');

    // bom (faster)
    $($sidebar[0]).find('ul');
    ```
