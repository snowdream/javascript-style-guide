##Пробелы

  - Используйте программную табуляцию (ее поддерживают все современные редакторы кода и IDE) из двух пробелов.

    ```javascript
    // плохо
    function() {
    ∙∙∙∙var name;
    }

    // плохо
    function() {
    ∙var name;
    }

    // хорошо
    function() {
    ∙∙var name;
    }
    ```
  - Устанавливайте один пробел перед открывающей скобкой.

    ```javascript
    // плохо
    function test(){
      console.log('test');
    }

    // хорошо
    function test() {
      console.log('test');
    }

    // плохо
    dog.set('attr',{
      age: '1 year',
      breed: 'Bernese Mountain Dog'
    });

    // хорошо
    dog.set('attr', {
      age: '1 year',
      breed: 'Bernese Mountain Dog'
    });
    ```
  - Оставляйте новую строку в конце файла.

    ```javascript
    // плохо
    (function(global) {
      // ...код...
    })(this);
    ```

    ```javascript
    // хорошо
    (function(global) {
      // ...код...
    })(this);

    ```

  - Используйте отступы, когда делаете цепочки вызовов.

    ```javascript
    // плохо
    $('#items').find('.selected').highlight().end().find('.open').updateCount();

    // хорошо
    $('#items')
      .find('.selected')
        .highlight()
        .end()
      .find('.open')
        .updateCount();

    // плохо
    var leds = stage.selectAll('.led').data(data).enter().append('svg:svg').class('led', true)
        .attr('width',  (radius + margin) * 2).append('svg:g')
        .attr('transform', 'translate(' + (radius + margin) + ',' + (radius + margin) + ')')
        .call(tron.led);

    // хорошо
    var leds = stage.selectAll('.led')
        .data(data)
      .enter().append('svg:svg')
        .class('led', true)
        .attr('width',  (radius + margin) * 2)
      .append('svg:g')
        .attr('transform', 'translate(' + (radius + margin) + ',' + (radius + margin) + ')')
        .call(tron.led);
    ```
