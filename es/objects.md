## Objetos

  - Usa la sintaxis literal para la creación de un objeto.

    ```javascript
    // mal
    var item = new Object();

    // bien
    var item = {};
    ```

  - No uses [palabras reservadas](http://es5.github.io/#x7.6.1) como nombres. No funciona en IE8. [Más información](https://github.com/airbnb/javascript/issues/61)

    ```javascript
    // mal
    var superman = {
      default: { clark: 'kent' },
      private: true
    };

    // bien
    var superman = {
      defaults: { clark: 'kent' },
      hidden: true
    };
    ```

  - Usa sinónimos legibles en lugar de palabras reservadas cuando sean necesarias.

    ```javascript
    // mal
    var superman = {
      class: 'alien'
    };

    // bien
    var superman = {
      klass: 'alien'
    };

    // bien
    var superman = {
      type: 'alien'
    };
    ```