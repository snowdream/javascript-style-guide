## Variables

  - Siempre usa `var` para declarar variables. No hacerlo resultará en variables globales. Debemos evitar contaminar el espacio global (global namespace). El Capitán Planeta nos advirtió de eso.

    ```javascript
    // mal
    superPower = new SuperPower();

    // bien
    var superPower = new SuperPower();
    ```

  - Usa una declaración `var` para múltiples variables y declara cada variable en una nueva línea.

    ```javascript
    // mal
    var items = getItems();
    var goSportsTeam = true;
    var dragonball = 'z';

    // bien
    var items = getItems(),
        goSportsTeam = true,
        dragonball = 'z';
    ```

  - Declara a las variables sin asignación al final. Esto es útil cuando necesites asignar una variable luego dependiendo de una de las variables asignadas previamente, lo hace más notorio.

    ```javascript
    // mal
    var i, len, dragonball,
        items = getItems(),
        goSportsTeam = true;

    // mal
    var i, items = getItems(),
        dragonball,
        goSportsTeam = true,
        len;

    // bien
    var items = getItems(),
        goSportsTeam = true,
        dragonball,
        length,
        i;
    ```

  - Asigna las variables al inicio de su ámbito. Esto ayuda a evitar inconvenientes con la declaración de variables y temas relacionados a 'hoisting'.

    ```javascript
    // mal
    function() {
      test();
      console.log('doing stuff..');

      //..otras cosas..

      var name = getName();

      if (name === 'test') {
        return false;
      }

      return name;
    }

    // bien
    function() {
      var name = getName();

      test();
      console.log('doing stuff..');

      //..otras cosas..

      if (name === 'test') {
        return false;
      }

      return name;
    }

    // mal
    function() {
      var name = getName();

      if (!arguments.length) {
        return false;
      }

      return true;
    }

    // bien
    function() {
      if (!arguments.length) {
        return false;
      }

      var name = getName();

      return true;
    }
    ```