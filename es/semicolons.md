## Puntos y Comas

  - **Sip.**

    ```javascript
    // mal
    (function() {
      var name = 'Skywalker'
      return name
    })()

    // bien
    (function() {
      var name = 'Skywalker';
      return name;
    })();

    // bien
    ;(function() {
      var name = 'Skywalker';
      return name;
    })();
    ```
