##Traductions

  Ce guide de style dans sa version originale :

  - :us: **Anglais** : [airbnb/javascript](https://github.com/airbnb/javascript)

  Et dans d'autres langues :

  - :de: **Allemand** : [timofurrer/javascript-style-guide](https://github.com/timofurrer/javascript-style-guide)
  - :jp: **Japonais** : [mitsuruog/javacript-style-guide](https://github.com/mitsuruog/javacript-style-guide)
  - :br: **Portugais** : [armoucar/javascript-style-guide](https://github.com/armoucar/javascript-style-guide)
  - :cn: **Chinois** : [adamlu/javascript-style-guide](https://github.com/adamlu/javascript-style-guide)
  - :es: **Espagnol** : [paolocarrasco/javascript-style-guide](https://github.com/paolocarrasco/javascript-style-guide)
  - :kr: **Coréen**: [tipjs/javascript-style-guide](https://github.com/tipjs/javascript-style-guide)