## Fonctions

  - Expressions de fonction :

    ```javascript
    // expression de fonction anonyme
    var anonymous = function() {
      return true;
    };

    // expression de fonction nommée
    var named = function named() {
      return true;
    };

    // expression de fonction immédiatement appelée (IIFE)
    (function() {
      console.log('Welcome to the Internet. Please follow me.');
    })();
    ```

  - Ne déclarez jamais une fonction dans un bloc non-foncition (if, while, etc). Assignez plutôt la fonction à une variable. Les navigateurs vous permettront de le faire, mais ils l'interprèteront tous différemment, et là c'est la caca, c'est la cata, c'est la catastrophe.
  - **Note :** ECMA-262 définit un `bloc` comme une série d'instructions. La déclaration d'une fonction n'est pas une instruction. [Lisez la note d'ECMA-262 sur ce problème](http://www.ecma-international.org/publications/files/ECMA-ST/Ecma-262.pdf#page=97).

    ```javascript
    // pas bien
    if (currentUser) {
      function test() {
        console.log('Nope.');
      }
    }

    // bien
    var test;
    if (currentUser) {
      test = function test() {
        console.log('Yup.');
      };
    }
    ```

  - Ne nommez jamais un paramètre `arguments`, cela prendra précédent sur l'objet `arguments` qui est donné dans la portée de toutes les fonctions.

    ```javascript
    // pas bien
    function nope(name, options, arguments) {
      // ...stuff...
    }

    // bien
    function yup(name, options, params) {
      // ...stuff...
    }
    ```