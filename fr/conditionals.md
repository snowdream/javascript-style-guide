## Expressions conditionnelles & Égalité

  - Préférez `===` et `!==` à `==` et `!=`.
  - Les expressions condtionnelles sont évaluées en utilisant les contrainte imposées par la méthode `ToBoolean` et suivent toujours ces simples rêgles :

    + Les **Objets** valent **true**
    + **Undefined** vaut **false**
    + **Null** vaut **false**
    + Les **Booleéns** valent **la valeur du booléen**
    + Les **Nombres** valent **false** si **+0, -0, ou NaN**, sinon **true**
    + Les **Strings** valent **false** si la string est vide `''`, sinon **true**

    ```javascript
    if ([0]) {
      // true
      // Un tableau est un objet, les objets valent true
    }
    ```

  - Utilisez des raccourcis.

    ```javascript
    // pas bien
    if (name !== '') {
      // ...stuff...
    }

    // bien
    if (name) {
      // ...stuff...
    }

    // pas bien
    if (collection.length > 0) {
      // ...stuff...
    }

    // bien
    if (collection.length) {
      // ...stuff...
    }
    ```

  - Pour plus d'information, lisez [Truth Equality and JavaScript](http://javascriptweblog.wordpress.com/2011/02/07/truth-equality-and-javascript/#more-2108) par Angus Croll