## Conversion de types & Contraintes

  - Faîtes vos contraintes de type au début de l'instruction.
  - Strings:

    ```javascript
    //  => this.reviewScore = 9;

    // pas bien
    var totalScore = this.reviewScore + '';

    // bien
    var totalScore = '' + this.reviewScore;

    // pas bien
    var totalScore = '' + this.reviewScore + ' total score';

    // bien
    var totalScore = this.reviewScore + ' total score';
    ```

  - Utilisez `parseInt` pour les Nombres et toujours avec la base numérique utilisée lors de la conversion.

    ```javascript
    var inputValue = '4';

    // pas bien
    var val = new Number(inputValue);

    // pas bien
    var val = +inputValue;

    // pas bien
    var val = inputValue >> 0;

    // pas bien
    var val = parseInt(inputValue);

    // bien
    var val = Number(inputValue);

    // bien
    var val = parseInt(inputValue, 10);
    ```

  - Si pour quelque raison que ce soit vous faîtes quelque chose de fou-fou, que `parseInt` vous ralentit et que vous devez utiliser le décallage de bits pour des [raisons de performances](http://jsperf.com/coercion-vs-casting/3), ajoutez un commentaire expliquant ce et pourquoi que vous le faîtes.
  - **Note :**  Soyez prudent lorsque vous utilisez les opérations de décallage de bits. Les Nombres sont représentés comme des [valeurs sur 64 bits](http://es5.github.io/#x4.3.19), mais les opérations de décallage de bits renvoient toujours des entiers sur 32 bits ([source](http://es5.github.io/#x11.7)). Les décallages de bits peuvent entraîner des comportements innatendus pour des valeurs entières stockées sur plus de 32 bits. [Discussion](https://github.com/airbnb/javascript/issues/109)

    ```javascript
    // bien
    /**
     * parseInt était la rasion pour laquelle mon code était lent.
     * Faire un décallage de bits sur la string pour la contraindre
     * à un Number l'a rendu beaucoup plus rapide.
     */
    var val = inputValue >> 0;
    ```

  - Booléens:

    ```javascript
    var age = 0;

    // pas bien
    var hasAge = new Boolean(age);

    // bien
    var hasAge = Boolean(age);

    // bien
    var hasAge = !!age;
    ```