## Strings

  - Utilisez les apostrophes (single quotes) `''` pour les strings

    ```javascript
    // pas bien
    var nom = "Bob Parr";

    // bien
    var nom = 'Bob Parr';

    // pas bien
    var nomComplet = "Bob " + this.nomDeFamille;

    // bien
    var nomComplet = 'Bob ' + this.nomDeFamille;
    ```

  - Les strings faisant plus de 80 caractères devraient être écrites sur plusieurs lignes, en utilisant la concaténation des chaînes de caractères.
  - Note : Si trop utilisée, la concaténation de strings trop longues peut influencer les performances. [jsPerf](http://jsperf.com/ya-string-concat) & [Discussion](https://github.com/airbnb/javascript/issues/40)

    ```javascript
    // pas bien
    var errorMessage = 'This is a super long error that was thrown because of Batman. When you stop to think about how Batman had anything to do with this, you would get nowhere fast.';

    // pas bien
    var errorMessage = 'This is a super long error that \
    was thrown because of Batman. \
    When you stop to think about \
    how Batman had anything to do \
    with this, you would get nowhere \
    fast.';


    // bien
    var errorMessage = 'This is a super long error that ' +
      'was thrown because of Batman.' +
      'When you stop to think about ' +
      'how Batman had anything to do ' +
      'with this, you would get nowhere ' +
      'fast.';
    ```

  - Quand vous contruisez programmatiquement une string, utilisez Array#join à la place de l'opérateur de concaténation. Principalement pour IE : [jsPerf](http://jsperf.com/string-vs-array-concat/2).

    ```javascript
    var objets,
        messages,
        longueur,
        i;

    messages = [{
        state: 'success',
        message: 'This one worked.'
    },{
        state: 'success',
        message: 'This one worked as well.'
    },{
        state: 'error',
        message: 'This one did not work.'
    }];

    longueur = messages.length;

    // pas bien
    function inbox(messages) {
      objets = '<ul>';

      for (i = 0; i < longueur; i++) {
        objets += '<li>' + messages[i].message + '</li>';
      }

      return objets + '</ul>';
    }

    // bien
    function inbox(messages) {
      objets = [];

      for (i = 0; i < longueur; i++) {
        objets[i] = messages[i].message;
      }

      return '<ul><li>' + objets.join('</li><li>') + '</li></ul>';
    }
    ```