##jQuery

  - Prefixez vos variables d'objets jQuery avec un `$`.

    ```javascript
    // pas bien
    var sidebar = $('.sidebar');

    // bien
    var $sidebar = $('.sidebar');
    ```

  - Mettez en cache vos requêtes jQuery.

    ```javascript
    // pas bien
    function setSidebar() {
      $('.sidebar').hide();

      // ...stuff...

      $('.sidebar').css({
        'background-color': 'pink'
      });
    }

    // bien
    function setSidebar() {
      var $sidebar = $('.sidebar');
      $sidebar.hide();

      // ...stuff...

      $sidebar.css({
        'background-color': 'pink'
      });
    }
    ```

  - Pour les requêtes du DOM, utilisez le sélecteur en cascades `$('.sidebar ul')` ou le parent > enfant `$('.sidebar > ul')`. [jsPerf](http://jsperf.com/jquery-find-vs-context-sel/16)
  - Utilisez `find` avec des objets de requêtes jQuery apparetenant à la portée.

    ```javascript
    // pas bien
    $('ul', '.sidebar').hide();

    // pas bien
    $('.sidebar').find('ul').hide();

    // bien
    $('.sidebar ul').hide();

    // bien
    $('.sidebar > ul').hide();

    // bien
    $sidebar.find('ul');
    ```
