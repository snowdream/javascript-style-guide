## Modules

  - Le module devrait commencer avec un `!`. Cela vous assure que, si un module malformé oublie d'ajouter un point virgule final, il n'y aura pas d'erreur en production lorsque les scripts seront concaténnés. [Explication](https://github.com/airbnb/javascript/issues/44#issuecomment-13063933)
  - Le fichier devrait être nommé avec la camelCase, se situer dans un dossier avec le même nom, et correspondre au nom du seul élément exporté.
  - Ajoutez une méthode nommée noConflict() qui restaure le module exporté à sa version précédente et le renvoit.
  - Déclarez toujours `'use strict';` au début du module.

    ```javascript
    // fancyInput/fancyInput.js

    !function(global) {
      'use strict';

      var previousFancyInput = global.FancyInput;

      function FancyInput(options) {
        this.options = options || {};
      }

      FancyInput.noConflict = function noConflict() {
        global.FancyInput = previousFancyInput;
        return FancyInput;
      };

      global.FancyInput = FancyInput;
    }(this);
    ```