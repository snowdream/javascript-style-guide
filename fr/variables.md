## Variables

  - Utilisez toujours `var` pour déclarer des variables. Ne par le faire entaîne dans la création de variables globales. Nous préfèrons éviter de poluer l'espace de noms global. Capitaine Planète nous a prévenu de ces dangers.

    ```javascript
    // pas bien
    superPower = new SuperPower();

    // bien
    var superPower = new SuperPower();
    ```

  - N'utiliez qu'une seule déclaration `var` pour de multiples variables et déclarez chacune d'entre elles sur une nouvelle ligne.

    ```javascript
    // pas bien
    var items = getItems();
    var goSportsTeam = true;
    var dragonball = 'z';

    // bien
    var items = getItems(),
        goSportsTeam = true,
        dragonball = 'z';
    ```

  - Déclarez les varibles indéfinies en dernier. Cela s'avère pratique lorsque plus tard vous aurez besoin d'assigner une variable en fonction d'une autre précédemment assignée.

    ```javascript
    // pas bien
    var i, len, dragonball,
        items = getItems(),
        goSportsTeam = true;

    // pas bien
    var i, items = getItems(),
        dragonball,
        goSportsTeam = true,
        len;

    // bien
    var items = getItems(),
        goSportsTeam = true,
        dragonball,
        length,
        i;
    ```

  - Assignez vos variables au début de leur portée. Cela vous aide à éviter des problèmes avec la déclaration des variables et ceux liés au hissage de leur affectation.

    ```javascript
    // pas bien
    function() {
      test();
      console.log('doing stuff..');

      //..d'autre trucs..

      var name = getName();

      if (name === 'test') {
        return false;
      }

      return name;
    }

    // bien
    function() {
      var name = getName();

      test();
      console.log('doing stuff..');

      //..d'autre trucs..

      if (name === 'test') {
        return false;
      }

      return name;
    }

    // pas bien
    function() {
      var name = getName();

      if (!arguments.length) {
        return false;
      }

      return true;
    }

    // bien
    function() {
      if (!arguments.length) {
        return false;
      }

      var name = getName();

      return true;
    }
    ```