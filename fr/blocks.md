## Blocs

  - Entourrez d'accollades tous vos blocs contenus sur plusieurs lignes.

    ```javascript
    // pas bien
    if (test)
      return false;

    // bien
    if (test) return false;

    // bien
    if (test) {
      return false;
    }

    // pas bien
    function() { return false; }

    // bien
    function() {
      return false;
    }
    ```
