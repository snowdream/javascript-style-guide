## Conventions de nommage

  - Évitez les noms ne faisant qu'une seule lettre. Soyez descriptifs dans votre déclaration.

    ```javascript
    // pas bien
    function q() {
      // ...stuff...
    }

    // bien
    function query() {
      // ..stuff..
    }
    ```

  - Utilisez la camelCase lorsque vous nomez vos objets, fonctions et instances

    ```javascript
    // pas bien
    var OBJEcttsssss = {};
    var this_is_my_object = {};
    function c() {};
    var u = new user({
      name: 'Bob Parr'
    });

    // bien
    var thisIsMyObject = {};
    function thisIsMyFunction() {};
    var user = new User({
      name: 'Bob Parr'
    });
    ```

  - Utilisez la PascalCase lorsque vous nommez vos constructeurs ou vos classes

    ```javascript
    // pas bien
    function user(options) {
      this.name = options.name;
    }

    var bad = new user({
      name: 'nope'
    });

    // bien
    function User(options) {
      this.name = options.name;
    }

    var good = new User({
      name: 'yup'
    });
    ```

  - Ajoutez un underscore `_` au début du nom de vos propriétés privées

    ```javascript
    // pas bien
    this.__firstName__ = 'Panda';
    this.firstName_ = 'Panda';

    // bien
    this._firstName = 'Panda';
    ```

  - Lorsque vous sauvegardez une référence à `this`, utilisez `_this`.

    ```javascript
    // pas bien
    function() {
      var self = this;
      return function() {
        console.log(self);
      };
    }

    // pas bien
    function() {
      var that = this;
      return function() {
        console.log(that);
      };
    }

    // bien
    function() {
      var _this = this;
      return function() {
        console.log(_this);
      };
    }
    ```

  - Nommez vos fonctions. Cela s'avère pratique lorsque vous étudiez la pile d'exécution.

    ```javascript
    // pas bien
    var log = function(msg) {
      console.log(msg);
    };

    // bien
    var log = function log(msg) {
      console.log(msg);
    };
    ```