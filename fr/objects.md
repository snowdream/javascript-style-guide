## Objets

  - Utilisez la syntaxe littérale pour la création d'un objet

    ```javascript
    // pas bien
    var objet = new Object();

    // bien
    var objet = {};
    ```

  - N'utilisez pas les [mots réservés](http://es5.github.io/#x7.6.1) comme clés. Cela ne marchera pas sur IE8. [Plus d'informations](https://github.com/airbnb/javascript/issues/61)

    ```javascript
    // pas bien
    var superman = {
      default: { clark: 'kent' },
      private: true
    };

    // bien
    var superman = {
      defaults: { clark: 'kent' },
      hidden: true
    };
    ```

  - Utilisez des synonymes lisibles à la place des mots réservés.

    ```javascript
    // pas bien
    var superman = {
      class: 'alien'
    };

    // pas bien
    var superman = {
      klass: 'alien'
    };

    // bien
    var superman = {
      type: 'alien'
    };
    ```