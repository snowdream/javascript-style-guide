## Propriétés

  - Utilisez la notation point lorsque vous accédez aux propriétés.

    ```javascript
    var luke = {
      jedi: true,
      age: 28
    };

    // pas bien
    var isJedi = luke['jedi'];

    // bien
    var isJedi = luke.jedi;
    ```

  - Utilisez la notation `[]` lorsque vous accédez à des propriétés à l'aide d'une variable.

    ```javascript
    var luke = {
      jedi: true,
      age: 28
    };

    function getProp(prop) {
      return luke[prop];
    }

    var isJedi = getProp('jedi');
    ```