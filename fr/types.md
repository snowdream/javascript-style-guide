## Types

  - **Primitifs**: Quand vous accédez à un type primitif, travaillez directement avec sa valeur

    + `string`
    + `number`
    + `boolean`
    + `null`
    + `undefined`

    ```javascript
    var foo = 1,
        bar = foo;

    bar = 9;

    console.log(foo, bar); // => 1, 9
    ```
  - **Complexe**: Quand vous accédez à un type complexe, travaillez avec une référence de sa valeur

    + `object`
    + `array`
    + `function`

    ```javascript
    var foo = [1, 2],
        bar = foo;

    bar[0] = 9;

    console.log(foo[0], bar[0]); // => 9, 9
    ```