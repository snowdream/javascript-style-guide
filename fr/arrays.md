## Tableaux

  - Utilisez la syntaxe littérale pour la création d'un objet

    ```javascript
    // pas bien
    var objets = new Array();

    // bien
    var objets = [];
    ```

  - Si vous ne connaissez pas la taille du tableau, utilisez Array#push.

    ```javascript
    var unTableau = [];


    // pas bien
    unTableau[unTableau.length] = 'abracadabra';

    // bien
    unTableau.push('abracadabra');
    ```

  - Quand vous devez copier un tableau, utilisez Array#slice. [jsPerf](http://jsperf.com/converting-arguments-to-an-array/7)

    ```javascript
    var len = objets.length,
        objetsCopie = [],
        i;

    // pas bien
    for (i = 0; i < len; i++) {
      objetsCopie[i] = objets[i];
    }

    // bien
    objetsCopie = objets.slice();
    ```

  - Pour convertir un objet semblable à un tableau en un tableau, utilisez Array#slice.

    ```javascript
    function trigger() {
      var args = Array.prototype.slice.call(arguments);
      ...
    }
    ```