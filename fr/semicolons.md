## Point-virgules

  - **Yup.**

    ```javascript
    // pas bien
    (function() {
      var name = 'Skywalker'
      return name
    })()

    // bien
    (function() {
      var name = 'Skywalker';
      return name;
    })();

    // bien
    ;(function() {
      var name = 'Skywalker';
      return name;
    })();
    ```