## Espaces

  - Utilisez deux espaces pour des tabulations "douces"

    ```javascript
    // pas bien
    function() {
    ∙∙∙∙var name;
    }

    // pas bien
    function() {
    ∙var name;
    }

    // bien
    function() {
    ∙∙var name;
    }
    ```
  - Placez un espace avant une accolade ouvrante.

    ```javascript
    // pas bien
    function test(){
      console.log('test');
    }

    // bien
    function test() {
      console.log('test');
    }

    // pas bien
    dog.set('attr',{
      age: '1 year',
      breed: 'Bernese Mountain Dog'
    });

    // bien
    dog.set('attr', {
      age: '1 year',
      breed: 'Bernese Mountain Dog'
    });
    ```
  - Ajoutez une nouvelle ligne vide à la fin du fichier.

    ```javascript
    // pas bien
    (function(global) {
      // ...stuff...
    })(this);
    ```

    ```javascript
    // bien
    (function(global) {
      // ...stuff...
    })(this);

    ```

  - Indentez vos longues chaînes de méthodes.

    ```javascript
    // pas bien
    $('#items').find('.selected').highlight().end().find('.open').updateCount();

    // bien
    $('#items')
      .find('.selected')
        .highlight()
        .end()
      .find('.open')
        .updateCount();

    // pas bien
    var leds = stage.selectAll('.led').data(data).enter().append('svg:svg').class('led', true)
        .attr('width',  (radius + margin) * 2).append('svg:g')
        .attr('transform', 'translate(' + (radius + margin) + ',' + (radius + margin) + ')')
        .call(tron.led);

    // bien
    var leds = stage.selectAll('.led')
        .data(data)
      .enter().append('svg:svg')
        .class('led', true)
        .attr('width',  (radius + margin) * 2)
      .append('svg:g')
        .attr('transform', 'translate(' + (radius + margin) + ',' + (radius + margin) + ')')
        .call(tron.led);
    ```