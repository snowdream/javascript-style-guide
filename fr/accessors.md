## Accesseurs

  - Les fonctions d'accesseur pour les propriétés ne sont pas obligatoires
  - Si vous faîtes des fonctions d'accès, utilisez getVal() et setVal('salut')

    ```javascript
    // pas bien
    dragon.age();

    // bien
    dragon.getAge();

    // pas bien
    dragon.age(25);

    // bien
    dragon.setAge(25);
    ```

  - Si la propriété est un booléen, utilisez isVal() ou hasVal()

    ```javascript
    // pas bien
    if (!dragon.age()) {
      return false;
    }

    // bien
    if (!dragon.hasAge()) {
      return false;
    }
    ```

  - Vous pouvez créez des fonctions get() et set(), mais restez cohérents.

    ```javascript
    function Jedi(options) {
      options || (options = {});
      var lightsaber = options.lightsaber || 'blue';
      this.set('lightsaber', lightsaber);
    }

    Jedi.prototype.set = function(key, val) {
      this[key] = val;
    };

    Jedi.prototype.get = function(key) {
      return this[key];
    };
    ```
