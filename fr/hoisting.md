## Hissage

  - Les déclarations de variables se font hisser jusqu'au début de leur portée, mais pas leur affectation.

    ```javascript
    // nous savons que cela ne marchera pas (en supposant
    // qu'il n'existe pas de variable globale notDefined)
    function example() {
      console.log(notDefined); // => déclanche une ReferenceError
    }

    // Déclarer une variable après que vous
    // y référenciez marchera grâce au
    // hissage de variable. Note : l'affectation
    // de la valeur `true` n'est pas hissée.
    function example() {
      console.log(declaredButNotAssigned); // => undefined
      var declaredButNotAssigned = true;
    }

    // L'interpréteur hisse la déclaration
    // de variable au début de la portée.
    // Ce qui veut dire que notre exemple pourrait être écrit :
    function example() {
      var declaredButNotAssigned;
      console.log(declaredButNotAssigned); // => undefined
      declaredButNotAssigned = true;
    }
    ```

  - Les expressions de fonctions anonymes hissent leur nom de variable, mais pas leur assignement.

    ```javascript
    function example() {
      console.log(anonymous); // => undefined

      anonymous(); // => TypeError anonymous is not a function

      var anonymous = function() {
        console.log('anonymous function expression');
      };
    }
    ```

  - Les expressions de fonctions nommées hissent leur nom de variable, pas le nom de la fonction ou son corps.

    ```javascript
    function example() {
      console.log(named); // => undefined

      named(); // => TypeError named is not a function

      superPower(); // => ReferenceError superPower is not defined

      var named = function superPower() {
        console.log('Flying');
      };
    }

    // il en est de même lorsque le nom de la fonction
    // est le même que celui de la variable.
    function example() {
      console.log(named); // => undefined

      named(); // => TypeError named is not a function

      var named = function named() {
        console.log('named');
      }
    }
    ```

  - Les déclarations de fonctions hissent leur nom et leur corps.

    ```javascript
    function example() {
      superPower(); // => Flying

      function superPower() {
        console.log('Flying');
      }
    }
    ```

  - Pour plus d'informations, référrez-vous à [JavaScript Scoping & Hoisting](http://www.adequatelygood.com/2010/2/JavaScript-Scoping-and-Hoisting) par [Ben Cherry](http://www.adequatelygood.com/)